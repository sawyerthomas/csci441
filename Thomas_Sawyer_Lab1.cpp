//Sawyer Thomas
//lab 1
// 1/22/18

//libraries
#include <iostream>
#include <string>
#include <cstdio>
#include "bitmap_image.hpp"
#include <sstream>
#include <vector>

using namespace std; //bad practice but makes this lab a little simpler
//--------------------------------------------------

//class contains values for each point
class Vector5 {
public:
    float x; //x and y values
    float y;
    float a; //color values 0-1
    float b;
    float c;

    //default Constructor
    Vector5() {
      x= 0;
      y=0;
      a= 0;
      b=0;
      c=0;

      //std::cout << "in Vector2 default constructor" << std::endl;
    }
  };

//--------------------------------------------------


//prompts user for point and color and adds values
void add_point(Vector5& v) {
  //cout << "getting point" << ".\n";
    string str ; //holds input

    getline (cin, str); //input from user
    vector<float> vect; //vector to hold parsed segments
    istringstream ss(str); //istringstream

    float i=0;

    while (ss >> i)
      {
        vect.push_back(i);

          if (ss.peek() == ','||ss.peek() == ':') //checks for identified values
              ss.ignore(); //ignores some values
      }

      /*
      cout << vect.size()<<std::endl; //for testing purposes
      for (i=0; i< vect.size(); i++)
      cout << vect.at(i)<<std::endl;
      */

      //inputs parsed values into point coordinates and color
            i=0;
            v.x=vect.at(i);
            i++;
            v.y=vect.at(i);
            i++;
            v.a=vect.at(i)*255;
            i++;
            v.b=vect.at(i)*255;
            i++;
            v.c=vect.at(i)*255;




}
//--------------------------------------------------

//finds minimum and maximum x and y values from 3 given points
void check_corner(Vector5& v1, int a[], int b[])
  {
  if(v1.x>b[0]) //checks value
  {
    b[0]=v1.x;
  }
  if(v1.y>b[1]) //checks value
  {
    b[1]=v1.y;
  }
  if(v1.x<a[0]) //checks value
  {
    a[0]=v1.x;
  }
  if(v1.y<a[1]) //checks value
  {
    a[1]=v1.y;
  }
}

//--------------------------------------------------
//part1
//creates white rectangle that surrounds the 3 given points
void make_rectangle(Vector5& v1, Vector5& v2, Vector5& v3 ) {

  int botleft[2]={200,200}; //200,200 is max point on image
  int topright[2]={0}; //0,0 is min value on image

  //finds smallest and largest x,y values from the 3 provided points
  check_corner(v1,botleft,topright);
  check_corner(v2,botleft,topright);
  check_corner(v3,botleft,topright);

  //used to test code
  //cout << "botleft: " <<botleft[0]<<", "<<botleft[1] <<".\n";
  //cout << "topright: " <<topright[0]<<", "<<topright[1] <<".\n";
  //cout << "making square" << ".\n";
  
  bitmap_image image(200,200);

     // set background to black
     image.set_all_channels(0, 0, 0);

     image_drawer draw(image);

     draw.pen_width(3);
     draw.pen_color(255, 255, 255); //white
     draw.rectangle(botleft[0], botleft[1], topright[0], topright[1]); //plots from max and min points found

     //fills in the whole box
     for(int i=botleft[0];i<topright[0];i++)
     {
       for(int j=botleft[1];j<topright[1];j++)
       {
         draw.plot_pixel (i, j); //plots every individual pixel
       }
     }

     image.save_image("output_square.bmp"); //saves image


}



//--------------------------------------------------
//part2
//takes 3 input points and fills in triangle using barycentric coords
void make_triangle(Vector5& v1, Vector5& v2, Vector5& v3 ) {

  //barycentric coords
  float alpha=0;
  float beta=0;
  float sigma=0;

  //bounding box variables
  int botleft[2]={200,200};
  int topright[2]={0};

  //creates bounding box
  check_corner(v1,botleft,topright);
  check_corner(v2,botleft,topright);
  check_corner(v3,botleft,topright);

  //for testing code
  //cout << "alpha: " <<alpha<<", beta: "<<beta <<", sigma: "<< sigma <<".\n";

  //creates image
  bitmap_image image(200,200);
  // set background to black
  image.set_all_channels(0, 0, 0);
  image_drawer draw(image);
  draw.pen_width(1);
  draw.pen_color(255, 255, 255); //white
  //fills in every pixel if within triangle bounds using barycentric coords
  for(int i=botleft[0];i<topright[0];i++)
  {
    for(int j=botleft[1];j<topright[1];j++)
    {
      //calculates sigma, beta and alpha
      sigma=((v2.y-v3.y)*(i-v3.x)+(v3.x-v2.x)*(j-v3.y))/((v2.y-v3.y)*(v1.x-v3.x)+(v3.x-v2.x)*(v1.y-v3.y));
      beta=((v3.y-v1.y)*(i-v3.x)+(v1.x-v3.x)*(j-v3.y))/((v2.y-v3.y)*(v1.x-v3.x)+(v3.x-v2.x)*(v1.y-v3.y));
      alpha=1-beta-sigma;
      if(alpha<1 && alpha>0 && beta<1 && beta>0 && sigma<1 && sigma>0)
      {
        draw.plot_pixel (i,j); //colors in pixel
      }
    }
  }


  image.save_image("output_triangle.bmp");

}

//--------------------------------------------------
//part3
//takes 3 input points and fills in triangle using barycentric coords with gradient
void make_color_triangle(Vector5& v1, Vector5& v2, Vector5& v3 ) {

  //barycentric coords
  float alpha=0;
  float beta=0;
  float sigma=0;

  //bounding box variables
  int botleft[2]={200,200};
  int topright[2]={0};

  //creates bounding box
  check_corner(v1,botleft,topright);
  check_corner(v2,botleft,topright);
  check_corner(v3,botleft,topright);


  //creates image
  bitmap_image image(200,200);

     // set background to black
  image.set_all_channels(0, 0, 0);
  image_drawer draw(image);

  draw.pen_width(1);

  //draw.plot_pixel (newx, newy);

  //fills in every pixel if within triangle bounds using barycentric coords
  for(int i=botleft[0];i<topright[0];i++)
  {
    for(int j=botleft[1];j<topright[1];j++)
    {
      //calculates sigma, beta and alpha
      sigma=((v2.y-v3.y)*(i-v3.x)+(v3.x-v2.x)*(j-v3.y))/((v2.y-v3.y)*(v1.x-v3.x)+(v3.x-v2.x)*(v1.y-v3.y));
      beta=((v3.y-v1.y)*(i-v3.x)+(v1.x-v3.x)*(j-v3.y))/((v2.y-v3.y)*(v1.x-v3.x)+(v3.x-v2.x)*(v1.y-v3.y));
      alpha=1-beta-sigma;
      if(alpha<1 && alpha>0 && beta<1 && beta>0 && sigma<1 && sigma>0)
      {
        //selects appropriate color within triangle
        draw.pen_color(v1.a*sigma+v2.a*beta+v3.a*alpha, v1.b*sigma+v2.b*beta+v3.b*alpha,v1.c*sigma+v2.c*beta+v3.c*alpha);
        draw.plot_pixel (i,j);
      }
    }
  }


  image.save_image("output_color.bmp");

}


//--------------------------------------------------
int main(int argc, char** argv) {

  cout << "LAB 1!!!! wooo!" << std::endl;
  //three points as triangle bounds
  Vector5 pt1;
  Vector5 pt2;
  Vector5 pt3;


  cout << "please enter three points (enter a point as x,y:r,g,b)" << ".\n";

  //calls add point function 3 times
  add_point(pt1);
  add_point(pt2);
  add_point(pt3);


  //returns results with format offered in lab description
  cout << "the points you entered are" << ".\n";
  cout << pt1.x << ","<<pt1.y<< ":"<< pt1.a << "," << pt1.b<< ","<< pt1.c << ".\n";
  cout << pt2.x << ","<<pt2.y<< ":"<< pt2.a << "," << pt2.b<< ","<< pt2.c << ".\n";
  cout << pt3.x << ","<<pt3.y<< ":"<< pt3.a << "," << pt3.b<< ","<< pt3.c << ".\n";
  cout << "sucess" << ".\n";

  //part 1
  make_rectangle(pt1,pt2,pt3);
  //part 2
  make_triangle(pt1,pt2,pt3);
  //part 3
  make_color_triangle(pt1,pt2,pt3);

  return 0;

}
